# ipyupload repo2docker

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/gl/oscar6echo%2Fipyupload-repo2docker/master?filepath=demo-ipyupload.ipynb)

This repo contains:

-   the necessary config information for mybinder to build a notebook server
-   a demo notebook for the custom widget [ipyupload](https://gitlab.com/oscar6echo/ipyupload)
